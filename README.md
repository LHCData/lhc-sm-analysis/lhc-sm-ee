# SIGMON Energy Extraction Analysis

This package contains Energy Extraction analyses for 13kA and 600A circuits.

## Structure
The project is structured as follows:

* [analyses](src/lhcsmee/analyses/) folder contains analysis classes for 13kA and 600A energy extraction analysis.
* [checks](src/lhcsmee/checks/) folder contains digital signals criteria and analysis logic.
* [notebooks](src/lhcsmee/notebooks/) folder contains HWC, operational and statistics notebooks for 13kA and 600A energy extraction.


## Installation
This package is published on [acc-py](https://acc-py-repo.cern.ch), under `lhcsmee`.

```
pip install lhcsmee
```
