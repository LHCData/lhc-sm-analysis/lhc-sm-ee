RELEASE NOTES
=============

Version: 1.0.1
- signal compared with the reference properly (scaling/offset bugfix): [SIGMON-448](https://its.cern.ch/jira/browse/SIGMON-448)
- the first version of the digital signal analysis written:  [SIGMON-458](https://its.cern.ch/jira/browse/SIGMON-458)


Version: 1.0.0
- EE analysis extracted to the separate module: [SIGMON-367](https://its.cern.ch/jira/browse/SIGMON-367)