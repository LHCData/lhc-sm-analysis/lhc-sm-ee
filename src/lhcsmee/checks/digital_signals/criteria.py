"""Contains the logic that builds the criteria for the EE digital signals analysis."""

from __future__ import annotations

import dataclasses
import math
from abc import ABC, abstractmethod
from collections.abc import Iterable, Sequence
from enum import Enum
from functools import reduce
from pathlib import Path

import pandas as pd

_ST_PREFIX = "Signal_prefix"
_SIGNAL_NAME = "ST_EE_Signal_Name"
_FROM = "Change_from"
_TO = "Change_to"
_CHANGE_TYPE = "Change_when_ms"
_PLUS_MS = "Plus_ms"
_MINUS_MS = "Minus_ms"
_LINK = "Link"
_CONST_AT = "Const_at"
_CRITERION = "Criterion"


@dataclasses.dataclass
class _Change:
    from_: int
    to: int
    at: float


class _CriterionLink(Enum):
    AND = "AND"
    OR = "OR"
    NO_LINK = ""


@dataclasses.dataclass
class _Criterion(ABC):
    link: _CriterionLink

    @abstractmethod
    def apply(self, signal: pd.Series) -> bool:
        pass


class _AlwaysTrueCriterion(_Criterion):
    def apply(self, signal: pd.Series) -> bool:
        return True


@dataclasses.dataclass
class _SingleChangeCriterion(_Criterion):
    from_: int
    to: int
    start_time: float
    end_time: float

    def apply(self, signal: pd.Series) -> bool:
        changes = _get_changes_in_signal(signal)
        if any(self.is_expected_change(change) and change.at < self.start_time for change in changes):
            return False  # we fail if the signal changes to the expected value before the start time
        relevant_changes = [
            change for change in changes if self.is_within_expected_interval(change) and self.is_expected_change(change)
        ]
        if len(relevant_changes) != 1:
            return False  # we fail if there is not exactly one change in the time range
        return self.is_expected_change(relevant_changes[0])

    def is_expected_change(self, change: _Change) -> bool:
        return change.from_ == self.from_ and self.to == change.to

    def is_within_expected_interval(self, change: _Change) -> bool:
        return self.start_time <= change.at <= self.end_time


@dataclasses.dataclass
class _ChangesCriterion(_Criterion):
    criteria: list[_SingleChangeCriterion]

    def apply(self, signal: pd.Series) -> bool:
        return all(criterion.apply(signal) for criterion in self.criteria)


@dataclasses.dataclass
class _ConstCriterion(_Criterion):
    value: int

    def apply(self, signal: pd.Series) -> bool:
        return bool((signal == self.value).all())


@dataclasses.dataclass
class _OrCriterion(_Criterion):
    left: _Criterion
    right: _Criterion

    def apply(self, signal: pd.Series) -> bool:
        return self.left.apply(signal) or self.right.apply(signal)


class Criteria:
    """Aggregates all the criteria for the EE digital signals analysis for RB"""

    def __init__(self, criteria_df: pd.DataFrame, source_name: str, circuit_type: str):
        self._criteria_df = criteria_df
        self.circuit_type = circuit_type
        self.signal_names = (
            [
                i + circuit_type[-1] + "." + source_name + ":" + j
                for i, j in zip(criteria_df[_ST_PREFIX], criteria_df[_SIGNAL_NAME], strict=True)
            ]
            if circuit_type != "A600"
            else [i for i in criteria_df[_SIGNAL_NAME]]
        )
        self._t_fpa2: float | None = None
        self._t_fpa1: float | None = None

    def apply(
        self, is_low_current: bool, signals: Iterable[pd.Series], unfiltered_signals: Iterable[pd.Series]
    ) -> Sequence[bool]:
        self._t_fpa1, self._t_fpa2 = _get_fpa1_fpa2(unfiltered_signals)
        criteria = self._build_criteria()
        _apply_specific_modifications(criteria)

        def meets_criteria(signal: pd.Series) -> bool:
            if self.circuit_type != "A600":
                signal_name = str(signal.name).split(":")[-1]
                # at low current, we ignore the ST_RES_OVERVOLT criterion
                if is_low_current and "ST_RES_OVERVOLT" in signal_name:
                    return True
                signal_prefix = str(signal.name).split(":")[0].split(".")[0][:-1]
                return criteria.loc[(signal_name, signal_prefix), _CRITERION].apply(signal)
            else:
                return criteria.loc[(signal.name, ""), _CRITERION].apply(signal)

        return [meets_criteria(signal) for signal in signals]

    def _build_criteria(self):
        criteria = self._criteria_df.copy()
        criteria[_CRITERION] = criteria.apply(self._build_criterion, axis=1)

        def join_criteria(left, right):
            criterion_from_link = {
                _CriterionLink.AND: lambda: _ChangesCriterion(_CriterionLink.NO_LINK, left.criteria + right.criteria),
                _CriterionLink.OR: lambda: _OrCriterion(_CriterionLink.NO_LINK, left, right),
            }
            criterion_from_link[left.link]()
            return criterion_from_link[left.link]()

        return criteria.groupby([_SIGNAL_NAME, _ST_PREFIX]).agg({_CRITERION: lambda s: reduce(join_criteria, s)})

    def _build_criterion(self, criteria_row) -> _Criterion:
        def _build_change_criterion(offset):
            return lambda: _ChangesCriterion(
                _CriterionLink(criteria_row[_LINK]),
                [
                    _SingleChangeCriterion(
                        _CriterionLink(criteria_row[_LINK]),
                        criteria_row[_FROM],
                        criteria_row[_TO],
                        (
                            criteria_row[_MINUS_MS] * -1e-3 + offset
                            if not pd.isna(criteria_row[_MINUS_MS])
                            else -math.inf
                        ),
                        criteria_row[_PLUS_MS] * 1e-3 + offset if not pd.isna(criteria_row[_PLUS_MS]) else math.inf,
                    )
                ],
            )

        criteria_by_change_type = {
            "PM_TRIGGER": _build_change_criterion(0),
            "FPA1": _build_change_criterion(self._t_fpa1),
            "FPA2": _build_change_criterion(self._t_fpa2),
            "CONST": lambda: _ConstCriterion(_CriterionLink(criteria_row[_LINK]), criteria_row[_CONST_AT]),
        }

        return criteria_by_change_type[criteria_row[_CHANGE_TYPE]]()


def get_13ka_criteria(circuit_type: str, source_name: str) -> Criteria:
    """Returns the default Criteria object for 13kA"""
    path = Path(__file__).parent / "criteria.csv"
    criteria_df = pd.read_csv(path, converters={_ST_PREFIX: str, _SIGNAL_NAME: str, _CHANGE_TYPE: str, _LINK: str})
    return Criteria(criteria_df, source_name, circuit_type)


def get_600a_criteria(source_name: str, is_sof: bool = False) -> Criteria:
    """Returns the default Criteria object for 600A"""
    common_criteria_path = Path(__file__).parent / "common_criteria_600A.csv"
    common_criteria_df = pd.read_csv(
        common_criteria_path, converters={_ST_PREFIX: str, _SIGNAL_NAME: str, _CHANGE_TYPE: str, _LINK: str}
    )
    if is_sof:
        specific_criteria_path = Path(__file__).parent / "criteria_600A_sof.csv"
    else:
        specific_criteria_path = Path(__file__).parent / "criteria_600A_non_sof.csv"
    specific_criteria_df = pd.read_csv(
        specific_criteria_path, converters={_ST_PREFIX: str, _SIGNAL_NAME: str, _CHANGE_TYPE: str, _LINK: str}
    )
    criteria_df = pd.concat([common_criteria_df, specific_criteria_df], ignore_index=True)
    return Criteria(criteria_df, source_name, "A600")


def _get_fpa1_fpa2(signals: Iterable[pd.Series]) -> tuple[float | None, float | None]:
    fpa_1 = None
    fpa_2 = None
    for signal in signals:
        if "ST_REC_FPA1" in str(signal.name) or "ST_FPA_REC_0" in str(signal.name):
            fpa_1 = _get_time_of_first_change(1, signal)
        if "ST_REC_FPA2" in str(signal.name) or "ST_FPA_REC_1" in str(signal.name):
            fpa_2 = _get_time_of_first_change(1, signal)
    if fpa_1 is None:
        return fpa_2, fpa_1
    if fpa_2 is None:
        return fpa_1, fpa_2
    return min(fpa_1, fpa_2), max(fpa_1, fpa_2)


def _get_time_of_first_change(to_: int, signal: pd.Series) -> float | None:
    # filter signal to only have values equal to `to_`
    mask = signal == to_
    return signal[mask].index[0] if not signal[mask].empty else None


def _apply_specific_modifications(criteria: pd.DataFrame):
    """Modifies the initial criteria (parsed from the csv file), so that they match
    `pm-qps13kA-analysis-module/src/java/cern/pm/analysis/qps/qps_13kA/analysis/DataQps13kAEEImpl.java`
    TODO:
    - add check for the iDump < 200A;
    - add special fix for ST_NO_OPEN_Mx_x;
    - FPA_1 and FPA_2 check only on first 30s;
    - FPA_1 and FPA_2
    - ST_RES_FAN_FAIL: 'I prefer in the future analysis to check it as follow:
    From 20 seconds after the first FPA, up to the end, the signal should be low (0). If 1 - WARNING message (email).
    The 20s time is needed for the cooling ventilators to reach their nominal rpms (AC asynchronous motors).'
    - ST_SNUB_FAIL: 'This is another non-safety-critical signal.
    In the current java application it seems not checked but only presented.
    For future: It should always be 0. If 1 --> WARNING message (email) only.'
    """
    criteria.loc["ST_RES_FAN_FAIL", _CRITERION] = [_AlwaysTrueCriterion(_CriterionLink.NO_LINK)]


def _get_changes_in_signal(signal: pd.Series) -> Sequence[_Change]:
    change_mask = signal.rolling(window=2).apply(lambda x: x[0] != x[1], raw=True)
    changes = []
    for idx, value in signal[change_mask == 1].dropna().items():
        if not isinstance(idx, int | float):
            raise ValueError(f"Expected an float index, got {idx}")
        changes.append(_Change(1 - value, value, idx))

    return changes
