def sanitize(name):
    """
    Method that sanitizes the whitespaces, colons and dots in a string (i.e. to make it a valid file name).
    """
    return name.replace(" ", "_").replace(":", "-")
