import logging

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.api.analysis import analysis
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time

from lhcsmee.analyses.on_13ka import EE13kAAnalogSignalsAnalysis, EE13kADigitalSignalsAnalysis

ALL_SECTORS = "ALL"
TEN_MINUTES = 10 * 60 * 1_000_000_000


class EEAnalysisStrategy:
    """
    Strategy class to register the EE digital and analog analyses for the EE components of a circuit.
    """

    def __init__(
        self,
        ee_names: list[str],
        circuit_name: str,
        circuit_type: str,
        start_timestamp,
        search_period: tuple[float, str],
        seen_quadrupole_names: list[str],
    ):
        self.ee_names = ee_names
        self.circuit_name = circuit_name
        self.circuit_type = circuit_type
        self.start_timestamp = start_timestamp
        self.search_period = search_period
        self.logger = logging.getLogger(__name__)
        self.class_name = "DQAMSNRQ" if signal_metadata.is_main_quadrupole(circuit_name) else "DQAMSNRB"
        self.prefixes = ["RQF", "RQD"] if signal_metadata.is_main_quadrupole(circuit_name) else ["ODD", "EVEN"]
        self.expected_nr_of_ee = 1 if signal_metadata.is_main_quadrupole(circuit_name) else 2
        self.validate_ee_names()
        self.seen_quadrupole_names = seen_quadrupole_names

    def filter_timestamps(self, timestamps: list[int]) -> list[int]:
        """
        Method to filter timestamps that are less than 10 minutes apart. We always keep the
        first of the close timestamps. The components can have 2 registered EE events in PM,
        but for digital and analog analysis we are just interested in the first logged event.

        :param timestamps: list of timestamps to be filtered

        :return: list of filtered timestamps
        """
        filtered_timestamps: list[int] = []
        if not timestamps:
            return []
        last_timestamp = timestamps[0]
        for timestamp in timestamps:
            if filtered_timestamps and abs(timestamp - last_timestamp) <= TEN_MINUTES:
                last_timestamp = timestamp
                continue
            filtered_timestamps.append(timestamp)
            last_timestamp = timestamp
        return filtered_timestamps

    def validate_pm_response(self, ee_pm_data_headers_df: pd.DataFrame, ee_name: str) -> bool:
        """
        Method to validate the EE PM data response from the query. If no events are found, we log a warning.

        :param ee_pm_data_headers_df: DataFrame with the EE PM data headers for the component
        :param ee_name: name of the EE part of the component

        :return: True if the response is nonempty, False otherwise
        """
        if ee_pm_data_headers_df.empty:
            self.logger.warning(
                f"{self.circuit_name} has no data for {ee_name} (Thus, the digital/analog analyses won't be "
                f"initialized for this component/circuit)."
            )
            return False
        return True

    def get_ee_timestamps_for_component(self, ee_name: str) -> list[int]:
        """
        Method to get the EE timestamps for a component from PM.

        :param ee_name: name of the EE part of the component

        :return: list of timestamps for the component if they exist, empty list otherwise
        """
        ee_pm_data_headers_df = query.query_pm_data_headers(
            "QPS", self.class_name, ee_name, self.start_timestamp, self.search_period
        )
        if not self.validate_pm_response(ee_pm_data_headers_df, ee_name):
            return []
        return ee_pm_data_headers_df["timestamp"].tolist()

    def get_id_for_component_analysis(self, timestamp: int, prefix: str = "") -> str:
        """
        Method to create the analysis id for the component. The id convention is:
        - quadrupoles:
            - analog: RQF.A34@2024-03-05 17:45:01.159
            - digital: RQ[F|D].A78@2024-03-05 17:47:20.520
        - dipoles:
            - analog: RB.A12@2024-03-05 17:42:09.444
            - digital: [ODD|EVEN]_RB.A78@2024-03-05 17:47:20.520

        :param timestamp: timestamp for the EE event
        :param prefix: prefix for the analysis id

        :return: analysis id for the component
        """
        if signal_metadata.is_main_quadrupole(self.circuit_name):
            return f"{prefix}.{self.circuit_name.split('.')[1]}@{Time.to_string_short(timestamp)}"
        if prefix:
            prefix += "_"
        return f"{prefix}{self.circuit_name}@{Time.to_string_short(timestamp)}"

    def get_analyses_for_pair(
        self, timestamp_1: int, timestamp_2: int
    ) -> tuple[EE13kAAnalogSignalsAnalysis, EE13kADigitalSignalsAnalysis, EE13kADigitalSignalsAnalysis]:
        """
        Method to register the digital and analog analyses for a pair
        of timestamps representing both components of an EE event.

        :param timestamp_1: EE timestamp for the first component
        :param timestamp_2: EE timestamp for the second component

        :return: analog analysis, digital analysis for the first component,
        digital analysis for the second component
        """
        if signal_metadata.is_main_quadrupole(self.circuit_name):
            modified_circuit_name_1 = "RQF" + "." + self.circuit_name.split(".")[1]
            modified_circuit_name_2 = "RQD" + "." + self.circuit_name.split(".")[1]
        else:
            modified_circuit_name_1 = self.circuit_name
            modified_circuit_name_2 = self.circuit_name

        analog_analysis = EE13kAAnalogSignalsAnalysis(
            self.get_id_for_component_analysis(
                timestamp_1,
                self.circuit_name.split(".")[0] if signal_metadata.is_main_quadrupole(self.circuit_name) else "",
            ),
            self.start_timestamp,
            self.circuit_name,
            self.ee_names,
            (timestamp_1, timestamp_2),
        )

        digital_1 = EE13kADigitalSignalsAnalysis(
            self.get_id_for_component_analysis(timestamp_1, self.prefixes[0]),
            self.circuit_type,
            modified_circuit_name_1,
            self.ee_names[0],
            timestamp_1,
        )

        digital_2 = EE13kADigitalSignalsAnalysis(
            self.get_id_for_component_analysis(timestamp_2, self.prefixes[1]),
            self.circuit_type,
            modified_circuit_name_2,
            self.ee_names[1],
            timestamp_2,
        )
        return analog_analysis, digital_1, digital_2

    def get_analysis_pairs(
        self, timestamps_component_1: list[int], timestamps_component_2: list[int]
    ) -> list[tuple[int, int]]:
        """
        Pair timestamps for the two components of the circuit. We exclude pairs
        that are within 10 minutes.

        :param timestamps_component_1: timestamps for the first component
        :param timestamps_component_2: timestamps for the second component

        :return: list of pairs of timestamps for the two components
        """
        analysis_pairs: list[tuple[int, int]] = []
        i, j = 0, 0

        while i < len(timestamps_component_1) and j < len(timestamps_component_2):
            t1 = timestamps_component_1[i]
            t2 = timestamps_component_2[j]

            if analysis_pairs:
                last_t1, last_t2 = analysis_pairs[-1]
                if t1 - last_t2 <= TEN_MINUTES:
                    self.logger.warning(
                        f"{self.circuit_name}: Excluding pair ({Time.to_string_short(t1)}, {Time.to_string_short(t2)}) "
                        f"as it is within 10 minutes from the previous pair "
                        f"({Time.to_string_short(last_t1)}, {Time.to_string_short(last_t2)})."
                    )
                    i += 1
                    j += 1
                    continue
                else:
                    analysis_pairs.append((t1, t2))
            else:
                analysis_pairs.append((t1, t2))
            i += 1
            j += 1

        return analysis_pairs

    def get_analyses_to_register(self) -> tuple[list[EE13kAAnalogSignalsAnalysis], list[EE13kADigitalSignalsAnalysis]]:
        """
        Method to register the digital and analog analyses for the given circuit.

        :return: list of analog analyses and list of digital analyses
        """
        ee_timestamps_1 = self.get_ee_timestamps_for_component(self.ee_names[0])
        if not ee_timestamps_1:
            return [], []
        ee_timestamps_1.sort()
        filtered_timestamps_1 = self.filter_timestamps(ee_timestamps_1)

        if signal_metadata.is_main_quadrupole(self.circuit_name):
            if not self.seen_quadrupole_names:
                # we don't want to register any analysis at the moment
                # if we don't have the other quadrupole component
                return [], []
            else:
                other_ee_name = self.seen_quadrupole_names[0]
                self.ee_names.append(other_ee_name)

        ee_timestamps_2 = self.get_ee_timestamps_for_component(self.ee_names[1])
        if not ee_timestamps_2:
            return [], []
        filtered_timestamps_2 = self.filter_timestamps(ee_timestamps_2)
        filtered_timestamps_2.sort()

        analysis_pairs = self.get_analysis_pairs(filtered_timestamps_1, filtered_timestamps_2)

        analog_analyses = []
        digital_analyses = []

        # registering the digital and analog analyses for this circuit
        for pair in analysis_pairs:
            analog_analysis, digital_first_analysis, digital_second_analysis = self.get_analyses_for_pair(
                pair[0], pair[1]
            )
            analog_analyses.append(analog_analysis)
            digital_analyses.extend([digital_first_analysis, digital_second_analysis])

        return analog_analyses, digital_analyses

    def validate_ee_names(self) -> bool:
        """
        Method to validate the number of EE names for the circuit. For dipoles,
        we expect 2 EE names, for quadrupoles 1.

        :return: True if the number of EE names is as expected, False otherwise
        """
        if len(self.ee_names) != self.expected_nr_of_ee:
            self.logger.warning(
                f"{self.circuit_name} doesn't have the expected number of EE names: {self.expected_nr_of_ee}"
            )
            return False
        return True


class EESignalsOutput:
    def __init__(self, start_timestamp: int, search_period: tuple[int, str] = (1000, "s"), sector: str = "ALL"):
        self.start_timestamp = start_timestamp
        self.search_period = search_period
        self.digital_manager = analysis.AnalysisManager()
        self.analog_manager = analysis.AnalysisManager()
        self.quadrupole_ee_names: dict[str, list[str]] = {}
        self.sector = sector
        self.logger = logging.getLogger(__name__)

    def _circuit_matches_sector(self, circuit_name: str) -> bool:
        """
        Method that checks whether the circuit matches the sector of interest.

        :param circuit_name: name of the circuit

        :return: True if the circuit matches the sector, False otherwise
        """
        return self.sector == ALL_SECTORS or signal_metadata.get_circuit_subsector(circuit_name) == self.sector

    def add_analyses_to_manager(self, analog_analyses, digital_analyses):
        """
        Method to add the digital and analog analyses to the respective managers.

        :param analog_analyses: list of analog analyses to be added
        :param digital_analyses: list of digital analyses to be added
        """
        for a in analog_analyses:
            self.analog_manager.register_analysis(a)
        for a in digital_analyses:
            self.digital_manager.register_analysis(a)

    def register_analysis(self, circuit_type: str, circuit_name: str) -> None:
        """
        Method that registers the digital and analog analyses for the given circuit.

        :param circuit_type: type of the circuit
        :param circuit_name: name of the circuit
        """
        ee_names = signal_metadata.get_ee_names(circuit_name, self.start_timestamp)
        if ee_names:
            circuit_name_suffix = circuit_name.split(".")[-1]
            # we are keeping track of the quadrupole EE names for the quadrupole circuits,
            # as we need to pair the components together into the same analog analysis.
            if signal_metadata.is_main_quadrupole(circuit_name) and circuit_name_suffix not in self.quadrupole_ee_names:
                self.quadrupole_ee_names[circuit_name_suffix] = []

            analog_analyses, digital_analyses = EEAnalysisStrategy(
                ee_names,
                circuit_name,
                circuit_type,
                self.start_timestamp,
                self.search_period,
                (
                    self.quadrupole_ee_names[circuit_name_suffix]
                    if signal_metadata.is_main_quadrupole(circuit_name)
                    else []
                ),
            ).get_analyses_to_register()

            if signal_metadata.is_main_quadrupole(circuit_name):
                self.quadrupole_ee_names[circuit_name_suffix].append(ee_names[0])

            self.add_analyses_to_manager(analog_analyses, digital_analyses)

    def register_analyses(self):
        """
        Method to register the digital and analog analyses for all the circuits in the sector.
        """
        circuit_types = signal_metadata.get_circuit_types()
        for circuit_type in circuit_types:
            circuit_names = signal_metadata.get_circuit_names(circuit_type)
            for circuit_name in circuit_names:
                if self._circuit_matches_sector(circuit_name):
                    self.register_analysis(circuit_type, circuit_name)
        self.validate_quadrupole_matches()

    def validate_quadrupole_matches(self):
        """
        Method that validates that both components of the quadrupoles have been registered.
        If not, we log a warning.
        """
        for circuit_name, registered_names in self.quadrupole_ee_names.items():
            if len(registered_names) != 2:
                self.logger.warning(
                    f"{circuit_name} has only one EE name registered, thus the analog analysis "
                    f"won't be initialized for this quadrupole circuit."
                )

    def get_output(self):
        """
        Method to get the output table with the results of the digital and analog analyses.

        :return: DataFrame with the results and specifics of the analyses
        """
        output_table = pd.DataFrame(
            columns=[
                "Circuit Name",
                "EE ODD/RQD Timestamp",
                "EE EVEN/RQF Timestamp",
                "Max voltage",
                "Analog result",
                "Digital result",
                "Overall result",
            ]
        )
        self.register_analyses()

        self.analog_manager.query_all()
        self.analog_manager.analyze_all()
        self.digital_manager.query_all()
        self.digital_manager.analyze_all()

        for idx, analysis_id in enumerate(self.analog_manager.get_registered_analyses_identifiers()):
            analog_analysis = self.analog_manager.get_analysis(analysis_id)
            if not isinstance(analog_analysis, EE13kAAnalogSignalsAnalysis):
                raise ValueError(f"Analysis {analysis_id} is not an analog analysis.")
            analog_analysis_result = analog_analysis.get_analysis_output()
            max_voltage = analog_analysis.max_voltage
            first_timestamp = 0
            second_timestamp = 0

            if "RB" in analysis_id:
                odd_analysis = self.digital_manager.get_analysis(f"ODD_{analysis_id}")
                if not isinstance(odd_analysis, EE13kADigitalSignalsAnalysis):
                    raise ValueError(f"Analysis ODD_{analysis_id} is not a digital analysis.")
                first_timestamp = odd_analysis.timestamp_ee
                odd_analysis_idx = self.digital_manager.get_registered_analyses_identifiers().index(
                    f"ODD_{analysis_id}"
                )
                even_analysis_id = self.digital_manager.get_registered_analyses_identifiers()[odd_analysis_idx + 1]
                even_analysis = self.digital_manager.get_analysis(even_analysis_id)
                if not isinstance(even_analysis, EE13kADigitalSignalsAnalysis):
                    raise ValueError(f"Analysis {even_analysis_id} is not a digital analysis.")
                second_timestamp = even_analysis.timestamp_ee

                digital_analysis_result = odd_analysis.get_analysis_output() and even_analysis.get_analysis_output()
                analysis_id = analysis_id.split("@")[0]

            if "RQ" in analysis_id:
                rqf_analysis_id = f"RQF.{analysis_id.split('.', 1)[1]}"
                rqf_analysis = self.digital_manager.get_analysis(rqf_analysis_id)
                if not isinstance(rqf_analysis, EE13kADigitalSignalsAnalysis):
                    raise ValueError(f"Analysis {rqf_analysis_id} is not a digital analysis.")
                first_timestamp = rqf_analysis.timestamp_ee
                rqf_analysis_idx = self.digital_manager.get_registered_analyses_identifiers().index(rqf_analysis_id)
                rqd_analysis_id = self.digital_manager.get_registered_analyses_identifiers()[rqf_analysis_idx + 1]
                rqd_analysis = self.digital_manager.get_analysis(rqd_analysis_id)
                if not isinstance(rqd_analysis, EE13kADigitalSignalsAnalysis):
                    raise ValueError(f"Analysis {rqd_analysis_id} is not a digital analysis.")
                second_timestamp = rqd_analysis.timestamp_ee
                sector = analysis_id.split("@")[0].split(".")[1]
                analysis_id = f"RQ.{sector}"

                digital_analysis_result = rqd_analysis.get_analysis_output() and rqf_analysis.get_analysis_output()

            overall_result = analog_analysis_result and digital_analysis_result
            output_table.loc[idx] = [
                analysis_id,
                Time.to_string_short(first_timestamp),
                Time.to_string_short(second_timestamp),
                max_voltage,
                analog_analysis_result,
                digital_analysis_result,
                overall_result,
            ]

        output_table = output_table.sort_values(by="EE ODD/RQD Timestamp").reset_index(drop=True)
        return output_table
