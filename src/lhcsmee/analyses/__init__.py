from __future__ import annotations

import logging

from lhcsmapi.api import analysis
from pyspark.sql.session import SparkSession

from lhcsmee.analyses.on_600a import EE600AAnalogSignalsAnalysis, EE600ADigitalSignalsAnalysis


class EE600ASignalsAnalysis(analysis.Analysis):
    """
    Wrapper class for the EE600AAnalogSignalsAnalysis and EE600ADigitalSignalsAnalysis classes.
    """

    def __init__(
        self, circuit_name: str, ee_name: str, ee_timestamp: int, is_sof: bool, logger: logging.Logger | None = None
    ):
        super().__init__(circuit_name, logger)
        self.ee_analog_analysis = EE600AAnalogSignalsAnalysis(
            circuit_name, circuit_name, ee_name, ee_timestamp, is_sof, logger=logger
        )
        self.ee_digital_analysis = EE600ADigitalSignalsAnalysis(
            circuit_name, circuit_name, ee_name, ee_timestamp, is_sof, logger=logger
        )

    def set_spark(self, spark: SparkSession):
        self.ee_analog_analysis.set_spark(spark)
        self.ee_digital_analysis.set_spark(spark)

    def query(self):
        super().query()
        self.ee_analog_analysis.query()
        self.ee_digital_analysis.query()

    def analyze(self):
        super().analyze()
        self.ee_analog_analysis.analyze()
        self.ee_digital_analysis.analyze()

    def get_analysis_output(self) -> bool:
        return self.ee_analog_analysis.get_analysis_output() and self.ee_digital_analysis.get_analysis_output()
