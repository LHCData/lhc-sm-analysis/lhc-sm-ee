# 13kA Energy Extraction Analysis

## Overview
When 13kA circuits are analyzed, the following components are analyzed in parallel:

* RB circuits (dipoles) contain 2 components: **ODD/EVEN**.
* RQ circuits (quadrupoles) contain 2 components: **RQF/RQD**.

## 1. Analog Signals Analysis

### 1.1. Min voltage check

The maximum voltage (`U_DUMP_RES`) of the odd/even components should be greater than **50V**. Otherwise, the subsequent checks are not executed (the analysis will be marked as **skipped**).

### 1.2. Tau value check

The tau value (time constant of the exponential decay) of the decay should be within:

* **(100, 130)** for RB circuits;
* **(25, 40)** for RQ circuits.

### 1.3. Maximum voltage difference check

The difference between the maximum voltage (`U_DUMP_RES`) value of the first component and of the second component should not be greater than **10%** of the maximum between the two.

### 1.4. Time difference between recorded EE events check

The difference between the recorded EE events in the two components should be between:

* **(0.45, 0.55) s** for RB circuits;
* **(-0.02, 0.02) s** for RQ circuits.


## 2. Digital Signals Analysis

All digital signals should be checked according to the file [criteria.csv](../../checks/digital_signals/criteria.csv). If there is low current (the voltage `U_DUMP_RES` does not exceed **20V**), then the analysis considers the `ST_RES_OVERVOLT` signal as passing all the time. The components are assessed separately, and then if both of them pass all digital checks, the check is marked as passing.

