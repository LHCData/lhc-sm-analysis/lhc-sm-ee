from __future__ import annotations

import logging

import lhcsmapi.signal_analysis.features as signal_analysis  # type: ignore
import lhcsmapi.signal_analysis.functions as signal_analysis_functions  # type: ignore
import pandas as pd
from lhcsmapi.api import analysis, query
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time

from lhcsmee.checks.digital_signals.criteria import get_13ka_criteria


class EE13kAAnalogSignalsAnalysis(analysis.Analysis):
    TAU_CHECK_TAG = "Tau value check"
    DURATION_DECAY = (1, 120)  # s
    TAU_RANGE_RB = (100, 130)
    TAU_RANGE_RQ = (25, 40)

    DIFFERENCE_BETWEEN_EE_TAG = "Time difference between recorded EE events check"
    MAX_DIFFERENCE_BETWEEN_RB_EES = (0.45, 0.55)  # s
    MAX_DIFFERENCE_BETWEEN_RQ_EES = (-0.02, 0.02)  # s

    DIFFERENCE_BETWEEN_MAXES_TAG = "Maximum voltage difference check"
    MIN_MAX_VOLTAGE_REQUIRED = 50  # V
    PERCENT_OF_MAX_VOLTAGE_VALUE = 0.1  # 10%

    def __init__(
        self,
        identifier: str,
        start_timestamp: int,
        circuit_name: str,
        ee_names: list[str],
        ee_timestamps: tuple[int, int],
        duration_decay: tuple[int, int] = DURATION_DECAY,
        verbose=False,
    ):
        super().__init__(identifier)
        self._start_timestamp = start_timestamp
        self._circuit_name = circuit_name
        self._ee_names = ee_names
        self.ee_timestamps = ee_timestamps
        self._duration_decay = duration_decay
        self._verbose = verbose
        self._logger.setLevel(logging.INFO)
        self.skip_flag = False
        self._max_difference_between_ee = (
            self.MAX_DIFFERENCE_BETWEEN_RB_EES
            if signal_metadata.is_main_dipole(self._circuit_name)
            else self.MAX_DIFFERENCE_BETWEEN_RQ_EES
        )
        self._tau_range = self.TAU_RANGE_RB if signal_metadata.is_main_dipole(self._circuit_name) else self.TAU_RANGE_RQ
        self._u_dumps: list[pd.Series] = []
        self._tau_check_odd_rqd = True
        self._tau_check_even_rqf = True

    def _query_u_dump(self):
        self._first_ee_timestamp = min(self.ee_timestamps)

        for idx, ee_name in enumerate(self._ee_names):
            if signal_metadata.is_main_quadrupole(self._circuit_name):
                signal = f"DQRQ.{ee_name}:U_DUMP_RES"
            else:
                signal = f"{ee_name}:U_DUMP_RES"

            self._u_dumps.append(
                query.query_pm_data_signals("QPS", self._class_name, ee_name, signal, self.ee_timestamps[idx])
            )

    def query(self):
        super().query()
        self._class_name = "DQAMSNRQ" if signal_metadata.is_main_quadrupole(self._circuit_name) else "DQAMSNRB"
        self._query_u_dump()

    def _check_difference_between_ee(self, log: logging.LoggerAdapter):
        (min_expected_difference, max_expected_difference) = self._max_difference_between_ee

        self.difference_in_seconds = abs(
            pd.to_datetime(self.ee_timestamps[0]) - pd.to_datetime(self.ee_timestamps[1])
        ).total_seconds()

        if not min_expected_difference < self.difference_in_seconds < max_expected_difference:
            log.error(
                f"Check failed: Difference between EE timestamps is not in range {self._max_difference_between_ee}, "
                f"difference: {self.difference_in_seconds:.3f} s"
            )
            log.info(
                f"\tTimestamps are: {Time.to_datetime(self.ee_timestamps[0])} and "
                f"{Time.to_datetime(self.ee_timestamps[1])}"
            )
            return False
        log.info(
            f"Check passed: Difference between EE timestamps is in range {self._max_difference_between_ee},"
            f"difference: {self.difference_in_seconds:.3f} s"
        )
        log.info(
            f"\tTimestamps are: {Time.to_datetime(self.ee_timestamps[0])} and {Time.to_datetime(self.ee_timestamps[1])}"
        )
        return True

    def _check_tau_value(self, log: logging.LoggerAdapter):
        result = True
        (min_value, max_value) = self._tau_range
        for idx, u_dump in enumerate(self._u_dumps):
            subtracted_min = signal_analysis.subtract_min_value(u_dump.to_frame())
            selected_duration = signal_analysis.take_window(subtracted_min, *self._duration_decay)
            tau_u_dump_res = signal_analysis.calculate_features(selected_duration, signal_analysis_functions.tau_energy)
            log.info(f"\tTau value: {tau_u_dump_res:.2f} for {u_dump.name}")
            if not min_value < tau_u_dump_res < max_value:
                if idx == 0:
                    self._tau_check_odd_rqd = False
                else:
                    self._tau_check_even_rqf = False
                log.error(f"\tTau value {tau_u_dump_res:.2f} is not in range {min_value, max_value}")
                result = False
        if result:
            log.info(f"Tau values are in range {min_value, max_value}")
        return result

    def _check_difference_between_maxes(self, log: logging.LoggerAdapter):
        max_difference = self.PERCENT_OF_MAX_VOLTAGE_VALUE * self.max_voltage

        if abs(self.max_voltage_comp_1 - self.max_voltage_comp_2) > max_difference:
            log.error(
                f"\tDifference between maxes is greater than {self.PERCENT_OF_MAX_VOLTAGE_VALUE:%} "
                f"percent of the max value, it is:"
                f"{abs(self.max_voltage_comp_1 - self.max_voltage_comp_2):.1f} V"
            )
            log.info(f"\tMax values are: {self.max_voltage_comp_1:.1f} V and {self.max_voltage_comp_2:.1f} V")
            return False
        log.info(
            f"\tDifference between maxes is less than {self.PERCENT_OF_MAX_VOLTAGE_VALUE:%} percent of the max value."
        )
        log.info(f"\tMax values are: {self.max_voltage_comp_1:.1f} V and {self.max_voltage_comp_2:.1f} V")
        return True

    def analyze(self):
        super().analyze()
        self.normalized_dumps = self._u_dumps.copy()

        for x in self.normalized_dumps:
            x.index -= self._first_ee_timestamp
        self.max_voltage_comp_1 = max(self._u_dumps[0].values)
        self.max_voltage_comp_2 = max(self._u_dumps[1].values)
        self.max_voltage = max(self.max_voltage_comp_1, self.max_voltage_comp_2)
        if min(self.max_voltage_comp_1, self.max_voltage_comp_2) < self.MIN_MAX_VOLTAGE_REQUIRED:
            self._logger.error(
                f"Max value of U_DUMP_RES is less than {self.MIN_MAX_VOLTAGE_REQUIRED}V, the max values are:  "
                f"{float(self.max_voltage_comp_1):.1f} V,"
                f"{float(self.max_voltage_comp_2):.1f} V"
            )
            self.skip_flag = True
            self.analysis_output = True
            return

        # Tau value check
        tau_check_logger_adapter = logging.LoggerAdapter(self._logger, {"tags": self.TAU_CHECK_TAG})
        self.tau_check = self._check_tau_value(tau_check_logger_adapter)

        # Maximum voltage difference check
        difference_between_maxes_logger_adapter = logging.LoggerAdapter(
            self._logger, {"tags": self.DIFFERENCE_BETWEEN_MAXES_TAG}
        )
        self.difference_between_maximums = self._check_difference_between_maxes(difference_between_maxes_logger_adapter)
        difference_between_ees_logger_adapter = logging.LoggerAdapter(
            self._logger, {"tags": self.DIFFERENCE_BETWEEN_EE_TAG}
        )

        # Time difference between recorded EE events check
        self.difference_between_ees = self._check_difference_between_ee(difference_between_ees_logger_adapter)

        self.analysis_output = all([self.tau_check, self.difference_between_maximums, self.difference_between_ees])

    def get_analysis_output(self) -> bool:
        return self.analysis_output


class EE13kADigitalSignalsAnalysis(analysis.Analysis):
    """Analyses the digital signals from the RB's and RQ's EE."""

    LOW_CURRENT_VOLTAGE_LIMIT = 20

    def __init__(self, identifier: str, circuit_type: str, circuit_name: str, ee_name: str, timestamp_ee: int):
        super().__init__(identifier)
        self._circuit_type = circuit_type
        self._circuit_name = circuit_name
        self.timestamp_ee = timestamp_ee
        self._class_name = "DQAMSNRQ" if signal_metadata.is_main_quadrupole(circuit_name) else "DQAMSNRB"
        self._source_name = ee_name
        self._criteria = get_13ka_criteria(circuit_type, self._source_name)
        self._is_low_current = False

    def query(self):
        super().query()

        # query the U_DUMP_RES signal for finding out the maximum voltage
        if signal_metadata.is_main_quadrupole(self._circuit_name):
            signal = f"DQRQ.{self._source_name}:U_DUMP_RES"
        else:
            signal = f"{self._source_name}:U_DUMP_RES"

        self._u_dump = query.query_pm_data_signals(
            "QPS", self._class_name, self._source_name, signal, self.timestamp_ee
        )
        signal_names = list(set(self._criteria.signal_names))
        self.signals = self._query_ee_digital(signal_names)
        self.signals_filtered = [
            signal.rolling(5, center=True).median().dropna().astype(int) for signal in self.signals
        ]

    def analyze(self) -> None:
        super().analyze()
        self.detect_low_current()
        results = self._criteria.apply(self._is_low_current, self.signals_filtered, self.signals)
        self.results = {signal.name: result for signal, result in zip(self.signals_filtered, results, strict=True)}

    def detect_low_current(self) -> None:
        self.max_voltage = max(self._u_dump.values)
        if self.max_voltage < self.LOW_CURRENT_VOLTAGE_LIMIT:
            self._is_low_current = True

    def get_analysis_output(self) -> bool:
        return all(self.results.values())

    def _query_ee_digital(self, signal_names: list[str]) -> list[pd.Series]:

        res = query.query_pm_data_signals("QPS", self._class_name, self._source_name, signal_names, self.timestamp_ee)
        for signal in res:
            signal.index -= self.timestamp_ee
            signal.index /= 1e9

        return [signal.astype(int) for signal in res]
