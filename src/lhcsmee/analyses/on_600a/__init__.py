from __future__ import annotations

import dataclasses
import logging
import math
from pathlib import Path

import pandas as pd
from lhcsmapi.api import analysis, query
from lhcsmapi.metadata import signal_metadata
from lhcsmpowering.analyses.commons import start_logging_new_check
from lhcsmpowering.checks import analog_checks

from lhcsmee.checks.digital_signals.criteria import get_600a_criteria


@dataclasses.dataclass(frozen=True)
class Parameters600AEE:
    u_cap_ab_values_and_offsets: list[tuple[int, int]]
    u_cap_z_values_and_offsets: list[tuple[int, int]]

    u_cap_ab_bounded_regions: dict[int | float, tuple[float, float]]
    u_cap_z_bounded_regions: dict[int | float, tuple[float, float]]

    voltage_decay_absolute_margin: float
    voltage_decay_bounds: tuple[int, int | None]

    time_interval_nxcals_query: int = 1_000_000_000  # ns
    pm_data_headers_initial_offset: int = 1_000_000_000  # ns
    pm_data_headers_query_duration: int = 2_000_000_000  # ns
    U_CAP_ABZ_ABSOLUTE_MARGIN: float = 35  # V

    time_interval_after_start_of_decay: int = 6_000_000_000  # ns
    max_current_end_of_decay: float = 1  # A
    min_voltage_peak_check: float = 15  # V
    min_current_peak_check: float = 1  # A
    max_voltage_deviation: float = 0.25  # 25%


class EE600AAnalogSignalsAnalysis(analysis.Analysis):

    EE_VOLTAGE_CHECK_TAG = "EE voltage check"
    EE_CURRENT_VALUE_CHECK_TAG = "EE current value check"
    EE_PEAK_VOLTAGE_VALUE_CHECK_TAG = "EE peak voltage value check"

    def __init__(
        self,
        identifier: str,
        circuit_name: str,
        ee_name: str,
        ee_timestamp: int,
        is_sof: bool = False,
        verbose=False,
        logger: logging.Logger | None = None,
    ):
        super().__init__(identifier, logger)
        self._circuit_name = circuit_name
        self._ee_name = ee_name
        self.timestamp_ee = ee_timestamp
        self._is_sof = is_sof
        self._verbose = verbose
        self._class_name = "DQAMSN600"

        self.sof_parameters = Parameters600AEE(
            u_cap_ab_values_and_offsets=[(170, -30_000_000)],  # Assertion 1 for capacitors A/B (170V, -30_000_000 ns)
            u_cap_z_values_and_offsets=[(170, -30_000_000)],  # Assertion 1 for capacitor Z (170V, -30_000_000 ns)
            u_cap_ab_bounded_regions={
                self.timestamp_ee  # Point 1: (35_000_000 ns after PM event, constraint: 0V, 55V + 35V)
                + 35_000_000: (0, 55 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN),
                self.timestamp_ee  # Point 2: (2_000_000_000 ns after PM event, constraint: 110V +- 35V)
                + 2_000_000_000: (
                    110 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    110 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
                self.timestamp_ee  # Point 3 (4_000_000_000 ns after PM event, constraint: 140V +- 35V)
                + 4_000_000_000: (
                    140 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    140 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
            },
            u_cap_z_bounded_regions={
                self.timestamp_ee  # Point 1: (60_000_000 ns after PM event, constraint: 0V, 55V + 35V)
                + 60_000_000: (0, 55 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN),
                self.timestamp_ee  # Point 2: (2_000_000_000 ns after PM event, constraint: 110V +- 35V)
                + 2_000_000_000: (
                    110 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    110 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
                self.timestamp_ee  # Point 3: (4_000_000_000 ns after PM event, constraint: 140V +- 35V)
                + 4_000_000_000: (
                    140 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    140 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
            },
            voltage_decay_absolute_margin=20,  # V
            voltage_decay_bounds=(15_000_000, None),  # ns
        )

        self.non_sof_parameters = Parameters600AEE(
            u_cap_ab_values_and_offsets=[(170, -30_000_000)],  # Assertion 1 for capacitors A/B (170V, -30_000_000 ns)
            u_cap_z_values_and_offsets=[],
            u_cap_ab_bounded_regions={
                self.timestamp_ee  # Point 1: (30_000_000 ns after PM event, constraint: 0V, 55V + 35V)
                + 30_000_000: (0, 55 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN),
                self.timestamp_ee  # Point 2: (2_000_000_000 ns after PM event, constraint: 110V +- 35V)
                + 2_000_000_000: (
                    110 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    110 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
                self.timestamp_ee  # Point 3: (4_000_000_000 ns after PM event, constraint: 140V +- 35V)
                + 4_000_000_000: (
                    140 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    140 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
            },
            u_cap_z_bounded_regions={
                # constant constraint across the whole interval: 170V +- 35V
                -math.inf: (
                    170 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    170 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
                math.inf: (
                    170 - Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    170 + Parameters600AEE.U_CAP_ABZ_ABSOLUTE_MARGIN,
                ),
            },
            voltage_decay_absolute_margin=50,  # V
            voltage_decay_bounds=(60_000_000, 300_000_000),  # ns
        )

        self.params = self.sof_parameters if self._is_sof else self.non_sof_parameters

    def query(self):
        (self.u_cap_a, self.u_cap_b, self.u_cap_z, self.u_dump_res) = query.query_pm_data_signals(
            "QPS", self._class_name, self._ee_name, ["U_CAP_A", "U_CAP_B", "U_CAP_Z", "U_DUMP_RES"], self.timestamp_ee
        )

        # I_MEAS
        [fgc] = signal_metadata.get_fgc_names(self._circuit_name, self.timestamp_ee)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self._circuit_name, self.timestamp_ee, "self")
        fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC",
            fgc_pm_class_name,
            fgc,
            self.timestamp_ee - self.params.pm_data_headers_initial_offset,
            self.params.pm_data_headers_query_duration,
        )
        self.timestamp_fgc = fgc_pm_data_headers["timestamp"].iat[0] if not fgc_pm_data_headers.empty else None
        if self.timestamp_fgc is None:
            self.i_meas = query.query_cmw_by_variables(
                self._spark,
                self.timestamp_ee - self.params.time_interval_nxcals_query,
                self.params.time_interval_nxcals_query,
                f"{fgc}:I_MEAS",
            )
        else:
            self.i_meas = query.query_pm_data_signals(
                "FGC", fgc_pm_class_name, fgc, "STATUS.I_MEAS", self.timestamp_fgc
            )

        self.max_voltage = max(abs(self.u_dump_res))
        req_values = pd.read_csv(Path(__file__).parent / "resistance_values_600AEE_circuits.csv")
        req_reference = req_values.loc[req_values["CIRCUIT_NAME"] == self._circuit_name]["Req_circuit [mΩ]"].values[0]
        max_current = max(abs(self.i_meas))
        self.voltage_reference = max_current * req_reference / 1e3  # Resistance in mili-Ohms

    def _check_voltage_within_absolute_margin(self):
        logger_adapter = start_logging_new_check(self._logger, self.EE_VOLTAGE_CHECK_TAG)
        return all(
            [
                analog_checks.check_signal_value_is_within_absolute_margin(
                    u_cap,
                    self.timestamp_ee,
                    value,
                    self.params.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    logger_adapter,
                    "V",
                    offset=offset,
                )
                for (value, offset) in self.params.u_cap_ab_values_and_offsets
                for u_cap in [self.u_cap_a, self.u_cap_b]
            ]
            + [
                analog_checks.check_signal_value_is_within_absolute_margin(
                    self.u_cap_z,
                    self.timestamp_ee,
                    value,
                    self.params.U_CAP_ABZ_ABSOLUTE_MARGIN,
                    logger_adapter,
                    "V",
                    offset=offset,
                )
                for (value, offset) in self.params.u_cap_z_values_and_offsets
            ]
        )

    def _check_voltage_within_bounded_region(self):
        logger_adapter = start_logging_new_check(self._logger, self.EE_VOLTAGE_CHECK_TAG)
        return all(
            (
                [
                    analog_checks.check_signal_is_within_bounded_region(
                        u_cap, self.params.u_cap_ab_bounded_regions, logger_adapter
                    )
                    for u_cap in [self.u_cap_a, self.u_cap_b]
                ]
            )
            + [
                analog_checks.check_signal_is_within_bounded_region(
                    self.u_cap_z, self.params.u_cap_z_bounded_regions, logger_adapter
                )
            ]
        )

    def _check_current_value(self):
        logger_adapter = start_logging_new_check(self._logger, self.EE_CURRENT_VALUE_CHECK_TAG)
        if (
            abs(self.i_meas.loc[self.timestamp_ee + self.params.time_interval_after_start_of_decay :]).max()
            > self.params.max_current_end_of_decay
        ):
            logger_adapter.error(
                f"Check failed: I_MEAS should decay to less than {self.params.max_current_end_of_decay} A "
                f"in the time period {self.params.time_interval_after_start_of_decay / 1e9} seconds "
                f"after the start of the decay."
            )
            return False

        logger_adapter.info(
            f"Check passed: I_MEAS is less than {self.params.max_current_end_of_decay} A "
            f"{self.params.time_interval_after_start_of_decay / 1e9} seconds "
            f"after the start of the decay."
        )
        return True

    def _check_peak_voltage_value(self):
        logger_adapter = start_logging_new_check(self._logger, self.EE_PEAK_VOLTAGE_VALUE_CHECK_TAG)
        if (
            self.max_voltage < self.params.min_voltage_peak_check
            and max(abs(self.i_meas)) < self.params.min_current_peak_check
        ):
            logger_adapter.info(
                f"Check skipped: Peak voltage value is less than {self.params.min_voltage_peak_check} V and"
                f" the maximum current value is less than {self.params.min_current_peak_check}."
            )
            return True

        if (
            (1 - self.params.max_voltage_deviation) * self.voltage_reference
            < self.max_voltage
            < (1 + self.params.max_voltage_deviation) * self.voltage_reference
        ):
            logger_adapter.info(
                f"Check passed: Peak voltage {self.max_voltage} V is within the expected range: "
                f"[{round((1 - self.params.max_voltage_deviation) * self.voltage_reference, 4)} V, "
                f"{round((1 + self.params.max_voltage_deviation) * self.voltage_reference, 4)}]."
            )
            return True
        logger_adapter.error(
            f"Check failed: Peak voltage value is not within the expected range. The expected value "
            f"is {self.voltage_reference:.2f} V, the actual value is {self.max_voltage:.2f} V."
        )
        return False

    def analyze(self):
        super().analyze()
        self.ee_voltage_check_wrt_absolute_margin = self._check_voltage_within_absolute_margin()
        self.ee_voltage_check_wrt_bounded_region = self._check_voltage_within_bounded_region()
        self.current_value_check = self._check_current_value()
        self.voltage_value_check = self._check_peak_voltage_value()

        self.analysis_output = all(
            [
                self.ee_voltage_check_wrt_absolute_margin,
                self.ee_voltage_check_wrt_bounded_region,
                self.current_value_check,
                self.voltage_value_check,
            ]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output


class EE600ADigitalSignalsAnalysis(analysis.Analysis):
    """Analyses the digital signals from the 600A's EE."""

    SAMPLING_INTERVAL = 20_000_000  # ns
    FPA_INTERVAL_CHECK_TAG = "FPA interval check"

    def __init__(
        self,
        identifier: str,
        circuit_name: str,
        ee_name: str,
        timestamp_ee: int,
        is_sof: bool = False,
        logger: logging.Logger | None = None,
    ):
        super().__init__(identifier, logger)
        self._circuit_name = circuit_name
        self.timestamp_ee = timestamp_ee
        self._is_sof = is_sof
        self._class_name = "DQAMSN600"
        self._source_name = ee_name
        self._criteria = get_600a_criteria(self._source_name, is_sof)
        self._is_low_current = False

    def query(self):
        super().query()

        signal_names = list(dict.fromkeys(self._criteria.signal_names))
        self.signals = self._query_ee_digital(signal_names)
        self.signals_filtered = [
            signal.rolling(5, center=True).median().dropna().astype(int) for signal in self.signals
        ]

    def analyze(self):
        super().analyze()
        signals = self.signals_filtered
        results = self._criteria.apply(self._is_low_current, signals, self.signals)
        self.result = {signal.name: result for signal, result in zip(signals, results, strict=True)}
        self.fpa_check_interval = self.fpa_interval_check()

    def fpa_interval_check(self):
        fpa_interval_check_adapter = logging.LoggerAdapter(self._logger, {"tags": self.FPA_INTERVAL_CHECK_TAG})
        if self._criteria._t_fpa1 is None or self._criteria._t_fpa2 is None:
            fpa_interval_check_adapter.error("\tCheck failed: The two FPAs are not recorded in the metadata.")
            return False
        interval_between_fpas = abs(self._criteria._t_fpa1 - self._criteria._t_fpa2)
        if interval_between_fpas > self.SAMPLING_INTERVAL:
            fpa_interval_check_adapter.error(
                f"\tCheck failed: The time interval between the two recorded FPAs is {interval_between_fpas},"
                " which is more than one sampling interval."
            )
            return False
        fpa_interval_check_adapter.info(
            "\tCheck passed: The two FPAs are recorded within at most one sampling interval between each other."
        )
        return True

    def get_analysis_output(self) -> bool:
        return self.fpa_check_interval and all(self.result.values())

    def _query_ee_digital(self, signal_names: list[str]) -> list[pd.Series]:

        res = query.query_pm_data_signals("QPS", self._class_name, self._source_name, signal_names, self.timestamp_ee)
        for signal in res:
            signal.index -= self.timestamp_ee
            signal.index /= 1e9

        return [signal.astype(int) for signal in res]
