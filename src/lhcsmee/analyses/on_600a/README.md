# 600A Energy Extraction Analysis

## Overview
For 600A circuits, there exist two types of situations that can happen during the analyzed event: 

* **SOF** (switch opening failure);
* **non-SOF**.

They are analyzed in a slightly different manner, and signify whether the Z capacitor is opening or not during energy extraction.

## 1. Analog Signals Analysis

### 1.1. Opening Checks

This check asserts that the `U_CAP_A`, `U_CAP_B` and `U_CAP_Z` signals of the three capacitors are within certain bounds on specific intervals defined by the following points (all points are relative to the EE timestamp):

* SOF:
    * `U_CAP_A/B`:
        * -30 ms: **170 V ± 35 V** (only this point)
        * Consecutive intervals defined by the following edges:
            * +35 ms: **0, 55 + 35 V**
            * +2 s: **110 ± 35 V**
            * +4 s: **140 ± 35 V**
    * `U_CAP_Z`:
        * -30 ms: **170 V ± 35 V** (only this point)
        * Consecutive intervals defined by the following edges:
            * +60 ms: **0, 55 + 35 V**
            * +2 s: **110 ± 35 V**
            * +4 s: **140 ± 35 V**

* non-SOF:
    * `U_CAP_A/B`:
        * -30 ms: **170 V ± 35 V** (only this point)
        * Consecutive intervals defined by the following edges:
            * +30 ms: **0, 55 + 35 V**
            * +2 s: **110 ± 35 V**
            * +4 s: **140 ± 35 V**
    * `U_CAP_Z`: The signal should be constant within **170 ± 35 V**.

### 1.2. Current Value Check at end of decay

This check verifies whether the maximum value of `I_MEAS` is below **1 A** from **6 s** after the start of the decay until the end of the signal.

### 1.3. Peak Voltage Value check

This check verifies whether the maximum value of `U_DUMP_RES` is within **±25%** of the expected peak voltage, calculated by multiplying the equivalent circuit resistance and the maximum value of `I_MEAS`. This check is only performed if the maximum value of `I_MEAS` is greater than **1 A**, and the maximum value of `U_DUMP_RES` is greater than **15 V**.

**Note:** For a negative current cycle, the absolute values are computed.

## 2. Digital Signals Analysis

### 2.1. FPA distance check

The two signals `ST_REC_FPA0` and `ST_REC_FPA1` should record the FPA change within one sampling interval (**20 ms**) from each other.

### 2.2. Digital signals checks

All filtered digital signals should be checked according to the files:

* common criteria: [common_criteria_600A.csv](../../checks/digital_signals/common_criteria_600A.csv)
* SOF criteria: [criteria_600A_sof.csv](../../checks/digital_signals/criteria_600A_sof.csv)
* non-SOF criteria: [criteria_600A_non_sof.csv](../../checks/digital_signals/criteria_600A_non_sof.csv)
