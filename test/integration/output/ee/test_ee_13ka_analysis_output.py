from lhcsmapi.Time import Time

from lhcsmee.output_utils.ee_13ka_analysis_output import EESignalsOutput


def test_ee_output():
    start_timestamp = Time.to_unix_timestamp("2024-06-12 14:00:00.000")
    search_period = (2 * 60 * 60, "s")

    output = EESignalsOutput(start_timestamp, search_period)

    html_output = output.get_output().to_html()
    assert html_output == (
        """<table border="1" class="dataframe">\n  <thead>\n    <tr style="text-align: right;">\n      """
        """<th></th>\n      <th>Circuit Name</th>\n      <th>EE ODD/RQD Timestamp</th>\n      """
        """<th>EE EVEN/RQF Timestamp</th>\n      <th>Max voltage</th>\n      <th>Analog result</th>\n      """
        """<th>Digital result</th>\n      <th>Overall result</th>\n    </tr>\n  </thead>\n  """
        """<tbody>\n    <tr>\n      <th>0</th>\n      <td>RQ.A34</td>\n      """
        """<td>2024-06-12 14:09:17.320</td>\n      <td>2024-06-12 14:09:17.321</td>\n      """
        """<td>0.4882</td>\n      <td>True</td>\n      <td>True</td>\n      <td>True</td>\n    """
        """</tr>\n    <tr>\n      <th>1</th>\n      <td>RB.A34</td>\n      """
        """<td>2024-06-12 14:09:47.522</td>\n      <td>2024-06-12 14:09:48.019</td>\n      """
        """<td>0.9764</td>\n      <td>True</td>\n      <td>True</td>\n      <td>True</td>\n    """
        """</tr>\n    <tr>\n      <th>2</th>\n      <td>RB.A67</td>\n      """
        """<td>2024-06-12 15:29:10.639</td>\n      <td>2024-06-12 15:29:11.145</td>\n      """
        """<td>0.7323</td>\n      <td>True</td>\n      <td>True</td>\n      <td>True</td>\n    """
        """</tr>\n    <tr>\n      <th>3</th>\n      <td>RB.A56</td>\n      """
        """<td>2024-06-12 15:29:30.177</td>\n      <td>2024-06-12 15:29:30.674</td>\n      """
        """<td>0.7323</td>\n      <td>True</td>\n      <td>True</td>\n      """
        """<td>True</td>\n    </tr>\n    <tr>\n      <th>4</th>\n      <td>RB.A23</td>\n      """
        """<td>2024-06-12 15:32:39.973</td>\n      <td>2024-06-12 15:32:40.476</td>\n      """
        """<td>3.6615</td>\n      <td>True</td>\n      <td>True</td>\n      <td>True</td>\n    """
        """</tr>\n    <tr>\n      <th>5</th>\n      <td>RB.A78</td>\n      """
        """<td>2024-06-12 15:39:04.310</td>\n      <td>2024-06-12 15:39:04.805</td>\n      """
        """<td>1.9528</td>\n      <td>True</td>\n      <td>True</td>\n      <td>True</td>\n    """
        """</tr>\n  </tbody>\n</table>"""
    )
