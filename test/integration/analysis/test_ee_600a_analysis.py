import pytest
from lhcsmapi.Time import Time
from nxcals.spark_session_builder import get_or_create

from lhcsmee.analyses import EE600ASignalsAnalysis


@pytest.mark.parametrize(
    ("circuit_name", "ee_name", "ee_timestamp", "is_sof", "logs"),
    [
        (
            "RCS.A12B1",
            "UA23.RCS.A12B1",
            1679378678386000000,
            True,
            [
                "EE voltage check",
                "\tCheck passed: Signal U_CAP_A was equal to 165.9 V -0.03 seconds after reference timestamp, which is within the target of 170.0 ± 35.0 V. Reference timestamp: 2023-03-21 07:04:38.386 (1679378678386000000).",
                "\tCheck passed: Signal U_CAP_B was equal to 168.0 V -0.03 seconds after reference timestamp, which is within the target of 170.0 ± 35.0 V. Reference timestamp: 2023-03-21 07:04:38.386 (1679378678386000000).",
                "\tCheck passed: Signal U_CAP_Z was equal to 167.5 V -0.03 seconds after reference timestamp, which is within the target of 170.0 ± 35.0 V. Reference timestamp: 2023-03-21 07:04:38.386 (1679378678386000000).",
                "EE voltage check",
                f"\tCheck passed: Signal U_CAP_A is within bounds between {Time.to_string_short(1679378678386000000 + 35_000_000)} and {Time.to_string_short(1679378678386000000 + 4_000_000_000)}.",
                f"\tCheck passed: Signal U_CAP_B is within bounds between {Time.to_string_short(1679378678386000000 + 35_000_000)} and {Time.to_string_short(1679378678386000000 + 4_000_000_000)}.",
                f"\tCheck passed: Signal U_CAP_Z is within bounds between {Time.to_string_short(1679378678386000000 + 60_000_000)} and {Time.to_string_short(1679378678386000000 + 4_000_000_000)}.",
                "EE current value check",
                "\tCheck passed: I_MEAS is less than 1 A 6.0 seconds after the start of the decay.",
                "EE peak voltage value check",
                "\tCheck passed: Peak voltage 133.71564 V is within the expected range: [107.6055 V, 179.3426].",
                "\tCheck passed: The two FPAs are recorded within at most one sampling interval between each other.",
            ],
        )
    ],
)
def test_600a(circuit_name, ee_name, ee_timestamp, is_sof, logs):
    # arrange
    spark = get_or_create()

    # act
    analysis = EE600ASignalsAnalysis(circuit_name, ee_name, ee_timestamp, is_sof)
    analysis.set_spark(spark)
    analysis.query()
    analysis.analyze()

    # assert
    assert analysis.get_analysis_output() is True
    expected_logs = logs
    messages = [i.message for i in analysis.get_logs()]
    assert messages == expected_logs
