from unittest.mock import MagicMock, patch

import pandas as pd
import pytest

from lhcsmee.analyses.on_13ka import EE13kAAnalogSignalsAnalysis


def mock_to_datetime(val):
    mock_date = MagicMock(spec=pd.Timestamp)
    timedelta_mock = MagicMock()
    timedelta_mock.total_seconds.return_value = 1000
    type(timedelta_mock).__abs__ = lambda self: timedelta_mock
    type(mock_date).__sub__ = lambda self, other: timedelta_mock
    return mock_date


@pytest.fixture
def setup_analysis():
    with (
        patch("lhcsmee.analyses.on_13ka.signal_metadata") as mock_metadata,
        patch("lhcsmee.analyses.on_13ka.query") as mock_query,
        patch("lhcsmee.analyses.on_13ka.signal_analysis") as mock_features,
        patch("lhcsmee.analyses.on_13ka.signal_analysis_functions") as mock_functions,
        patch("lhcsmee.analyses.on_13ka.pd.to_datetime", side_effect=mock_to_datetime),
        patch("lhcsmee.analyses.on_13ka.logging.Logger") as mock_logger,
        patch("lhcsmee.analyses.on_13ka.logging.LoggerAdapter") as mock_logger_adapter,
    ):
        mock_metadata.is_main_quadrupole.return_value = True
        mock_metadata.get_ee_names.return_value = ["EE1", "EE2"]
        mock_query.query_pm_data_signals.return_value = MagicMock(name="DataFrame")
        mock_logger.return_value = MagicMock()
        mock_logger_adapter.return_value = mock_logger.return_value

        analysis_instance = EE13kAAnalogSignalsAnalysis(
            identifier="test",
            start_timestamp=123456789,
            circuit_name="Circuit1",
            ee_names=["EE1", "EE2"],
            ee_timestamps=(123456790, 123456800),
        )
        yield analysis_instance, mock_query, mock_features, mock_functions, mock_metadata


def test_initialization(setup_analysis):
    analysis_instance, _, _, _, _ = setup_analysis
    assert analysis_instance._start_timestamp == 123456789
    assert analysis_instance._circuit_name == "Circuit1"
    assert not analysis_instance._verbose


def test_query(setup_analysis):
    analysis_instance, mock_query, _, _, mock_metadata = setup_analysis
    analysis_instance.query()
    mock_query.query_pm_data_signals.assert_called()
    assert analysis_instance._ee_names == ["EE1", "EE2"]


def test_check_difference_between_ee(setup_analysis):
    analysis_instance, _, _, _, _ = setup_analysis
    analysis_instance._max_difference_between_ee = (0.10, 0.20)
    analysis_instance._ee_timestamps = (1609459200, 1609459260)
    result = analysis_instance._check_difference_between_ee(analysis_instance._logger)
    expected_result = False
    assert result == expected_result


def test_check_tau_value(setup_analysis):
    analysis_instance, _, mock_features, _, _ = setup_analysis
    analysis_instance._u_dumps = [pd.Series({"value": [1, 2, 3, 4, 5]}), pd.Series({"value": [2, 3, 4, 5, 6]})]
    analysis_instance._tau_range = (100, 120)
    mock_features.subtract_min_value.return_value = MagicMock()
    mock_features.take_window.return_value = MagicMock()
    mock_features.calculate_features.return_value = 150
    result = analysis_instance._check_tau_value(analysis_instance._logger)
    assert result is False


def test_analyze(setup_analysis):
    analysis_instance, _, _, _, _ = setup_analysis
    analysis_instance._check_tau_value = MagicMock(return_value=True)
    analysis_instance._check_difference_between_maxes = MagicMock(return_value=True)
    analysis_instance._u_dumps = [MagicMock(), MagicMock()]
    analysis_instance._u_dumps[0].values = [100]
    analysis_instance._u_dumps[1].values = [100]
    analysis_instance.analyze()
    assert isinstance(analysis_instance.get_analysis_output(), bool)
