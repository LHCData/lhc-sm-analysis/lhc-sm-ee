import json
from io import StringIO
from pathlib import Path
from unittest.mock import patch

import pandas as pd
import pytest

from lhcsmee.analyses.on_13ka import EE13kADigitalSignalsAnalysis
from lhcsmee.analyses.on_600a import EE600ADigitalSignalsAnalysis
from lhcsmee.checks.digital_signals.criteria import Criteria

_RESOURCE_PATH = Path(__file__).parent / "digital_signals"

_PARAMS13kA = [
    ("RB", "RB.A23", 1666308385189000000, True),  # positive
    ("RB", "RB.A45", 1668360274691000000, False),  # negative ST_U_CAP_C2_LOW case
    ("RB", "RB.A45", 1668360274199000000, True),  # positive
    ("RB", "RB.A12", 1620831556570000000, True),  # positive
    ("RB", "RB.A12", 1620831557066000000, False),  # negative ST_U_CAP_C1_LOW case
    ("RB", "RB.A23", 1632850478271000000, True),
    ("RB", "RB.A23", 1632850478777000000, True),
]


def read_dfs_from_csv(file_path):
    with open(file_path) as file:
        content = file.read()

    # the file contains multiple dataframes separated by a line of dashes
    chunks = content.split("-------------------------\n,")
    dataframes = []
    for chunk in chunks[1:]:  # skip the first empty chunk (before the first dashed line)
        chunk_io = StringIO(chunk)
        df = pd.read_csv(chunk_io, index_col=0)
        dataframes.append(df.iloc[:, 0])

    return dataframes


@pytest.mark.parametrize(("circuit_type", "circuit_name", "timestamp_ee", "expected_result"), _PARAMS13kA)
def test_digital_signals_analysis_13ka(circuit_type, circuit_name, timestamp_ee, expected_result):
    analysis = EE13kADigitalSignalsAnalysis("", circuit_type, circuit_name, "ee_name", timestamp_ee)
    dfs = read_dfs_from_csv(_RESOURCE_PATH / f"{circuit_name}_{timestamp_ee}_query.csv")
    for df in dfs:
        df.index = pd.to_numeric(df.index, errors="raise")
    with patch("lhcsmapi.api.query.query_pm_data_signals", side_effect=[dfs[0], dfs]):
        analysis.query()
    analysis.analyze()
    with open(_RESOURCE_PATH / f"{circuit_name}_{timestamp_ee}_results.json") as file:
        expected_partial_results = json.load(file)
    assert analysis.results == expected_partial_results
    assert analysis.get_analysis_output() is expected_result


_PARAMS600A = [("RQTD.A12B1", 1679338723235000000, False), ("RCS.A12B1", 1679378678386000000, True)]


@pytest.mark.parametrize(("circuit_name", "timestamp_ee", "is_sof"), _PARAMS600A)
def test_digital_signals_analysis_600a(circuit_name, timestamp_ee, is_sof):
    analysis = EE600ADigitalSignalsAnalysis("", circuit_name, "ee_name", timestamp_ee, is_sof)
    dfs = read_dfs_from_csv(_RESOURCE_PATH / f"{circuit_name}_{timestamp_ee}_query.csv")
    for df in dfs:
        df.index = pd.to_numeric(df.index, errors="raise")
    with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=dfs):
        analysis.query()
    analysis.analyze()
    with open(_RESOURCE_PATH / f"{circuit_name}_{timestamp_ee}_results.json") as file:
        expected_partial_results = json.load(file)
    assert analysis.result == expected_partial_results
    assert analysis.get_analysis_output() is True


_SIGNAL_1 = "signal1"
_SIGNAL_2 = "signal2"

_PARAMS_CONST = [
    (0, pd.Series([0, 0, 0, 0, 0], name=_SIGNAL_1), True),
    (0, pd.Series([0, 1, 0, 0, 0], name=_SIGNAL_1), False),
    (0, pd.Series([1, 1, 1, 1, 1], name=_SIGNAL_1), False),
    (1, pd.Series([0, 0, 0, 0, 0], name=_SIGNAL_1), False),
    (1, pd.Series([1, 1, 1, 1, 1], name=_SIGNAL_1), True),
]


@pytest.mark.parametrize(("const_at", "signal", "expected"), _PARAMS_CONST)
def test_criteria_const_signal(const_at, signal, expected):
    criteria_df = pd.DataFrame(
        {
            "Signal_prefix": "signal",
            "ST_EE_Signal_Name": [_SIGNAL_1],
            "Change_from": [pd.NA],
            "Change_to": [pd.NA],
            "Change_when_ms": ["CONST"],
            "Plus_ms": [pd.NA],
            "Minus_ms": [pd.NA],
            "Link": [""],
            "Const_at": [const_at],
        }
    )

    with patch("lhcsmee.checks.digital_signals.criteria._get_fpa1_fpa2", return_value=(None, None)):
        criteria = Criteria(criteria_df, "source", "circuit_type")
        result = criteria.apply(False, [signal], [signal])
    assert result[0] is expected


_FPA_1 = 2
_FPA_2 = 4

index = [i * 0.005 for i in range(10)]
index_fpas = [i for i in range(10)]

_PARAMS_CHANGE_PM_TRIGGER = [
    ("PM_TRIGGER", 0, 1, 2000, 0, pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index), False),
    ("PM_TRIGGER", 0, 1, 2000, 0, pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1, index=index), False),
    ("PM_TRIGGER", 0, 1, 2000, 0, pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1, index=index), True),
    ("PM_TRIGGER", 0, 1, 2000, 0, pd.Series([0, 0, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1, index=index), True),
    (
        "PM_TRIGGER",
        0,
        1,
        2000,
        0,
        pd.Series([0, 0, 1, 1, 1, 1, 1, 0, 0, 1], name=_SIGNAL_1, index=index),
        False,
    ),  # noisy example
    (
        "PM_TRIGGER",
        0,
        1,
        2000,
        0,
        pd.Series([0, 0, 1, 1, 1, 1, 1, 0, 1, 0], name=_SIGNAL_1, index=index),
        False,
    ),  # noisy example
]

_PARAMS_CHANGE_FPA_1 = [
    ("FPA1", 0, 1, 2000, 1000, pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), False),
    ("FPA1", 1, 0, 2000, 1000, pd.Series([1, 0, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    ("FPA1", 1, 0, 2000, 1000, pd.Series([1, 1, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    ("FPA1", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    ("FPA1", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 1, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    (
        "FPA1",
        1,
        0,
        2000,
        1000,
        pd.Series([1, 0, 1, 0, 0, 0, 0, 0, 0, 1], name=_SIGNAL_1, index=index_fpas),
        False,
    ),  # noisy example
    (
        "FPA1",
        1,
        0,
        2000,
        1000,
        pd.Series([1, 1, 1, 1, 1, 0, 1, 0, 1, 0], name=_SIGNAL_1, index=index_fpas),
        False,
    ),  # noisy example
    ("FPA1", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 0], name=_SIGNAL_1, index=index_fpas), False),
]

_PARAMS_CHANGE_FPA_2 = [
    ("FPA2", 0, 1, 2000, 1000, pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), False),
    ("FPA2", 1, 0, 2000, 1000, pd.Series([1, 1, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), False),
    ("FPA2", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    ("FPA2", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 1, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    ("FPA2", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 1, 1, 0, 0, 0, 0, 0], name=_SIGNAL_1, index=index_fpas), True),
    (
        "FPA2",
        1,
        0,
        2000,
        1000,
        pd.Series([1, 1, 0, 1, 1, 0, 1, 0, 0, 1], name=_SIGNAL_1, index=index_fpas),
        False,
    ),  # noisy example
    (
        "FPA2",
        1,
        0,
        2000,
        1000,
        pd.Series([1, 0, 1, 1, 1, 1, 0, 0, 1, 0], name=_SIGNAL_1, index=index_fpas),
        False,
    ),  # noisy example
    ("FPA2", 1, 0, 2000, 1000, pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 0], name=_SIGNAL_1, index=index_fpas), False),
]

_PARAMS_CHANGE = _PARAMS_CHANGE_PM_TRIGGER + _PARAMS_CHANGE_FPA_1 + _PARAMS_CHANGE_FPA_2


@pytest.mark.parametrize(("change", "from_", "to_", "plus", "minus", "signal", "expected"), _PARAMS_CHANGE)
def test_criteria_change_at_signal(change, from_, to_, plus, minus, signal, expected):
    criteria_df = pd.DataFrame(
        {
            "Signal_prefix": "signal",
            "ST_EE_Signal_Name": [_SIGNAL_1],
            "Change_from": [from_],
            "Change_to": [to_],
            "Change_when_ms": [change],
            "Plus_ms": [plus],
            "Minus_ms": [minus],
            "Link": [""],
            "Const_at": [pd.NA],
        }
    )

    with patch("lhcsmee.checks.digital_signals.criteria._get_fpa1_fpa2", return_value=(_FPA_1, _FPA_2)):
        criteria = Criteria(criteria_df, "source", "circuit_type")
        result = criteria.apply(False, [signal], [signal])
    assert result[0] == expected


_PARAMS_CRITERIA_OR = [
    (
        pd.DataFrame(
            {
                "Signal_prefix": "signal",
                "ST_EE_Signal_Name": [_SIGNAL_1, _SIGNAL_1],
                "Change_from": [0, pd.NA],
                "Change_to": [1, pd.NA],
                "Change_when_ms": ["PM_TRIGGER", "CONST"],
                "Plus_ms": [1000, pd.NA],
                "Minus_ms": [1000, pd.NA],
                "Link": ["OR", "OR"],
                "Const_at": [pd.NA, 1],
            }
        ),
        pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1),
        True,
    ),
    (
        pd.DataFrame(
            {
                "Signal_prefix": "signal",
                "ST_EE_Signal_Name": [_SIGNAL_1, _SIGNAL_1],
                "Change_from": [0, pd.NA],
                "Change_to": [1, pd.NA],
                "Change_when_ms": ["PM_TRIGGER", "CONST"],
                "Plus_ms": [1000, pd.NA],
                "Minus_ms": [1000, pd.NA],
                "Link": ["OR", "OR"],
                "Const_at": [pd.NA, 1],
            }
        ),
        pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1),
        True,
    ),
    (
        pd.DataFrame(
            {
                "Signal_prefix": "signal",
                "ST_EE_Signal_Name": [_SIGNAL_1, _SIGNAL_1],
                "Change_from": [0, pd.NA],
                "Change_to": [1, pd.NA],
                "Change_when_ms": ["PM_TRIGGER", "CONST"],
                "Plus_ms": [1000, pd.NA],
                "Minus_ms": [1000, pd.NA],
                "Link": ["OR", "OR"],
                "Const_at": [pd.NA, 1],
            }
        ),
        pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1),
        True,
    ),
]

_PARAMS_CRITERIA_AND = [
    (
        pd.DataFrame(
            {
                "Signal_prefix": "signal",
                "ST_EE_Signal_Name": [_SIGNAL_1, _SIGNAL_1],
                "Change_from": [0, 1],
                "Change_to": [1, 0],
                "Change_when_ms": ["PM_TRIGGER", "FPA1"],
                "Plus_ms": [1000, 1000],
                "Minus_ms": [1000, 1000],
                "Link": ["AND", "AND"],
                "Const_at": [pd.NA, 1],
            }
        ),
        pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1),
        False,
    ),
    (
        pd.DataFrame(
            {
                "Signal_prefix": "signal",
                "ST_EE_Signal_Name": [_SIGNAL_1, _SIGNAL_1],
                "Change_from": [0, 1],
                "Change_to": [1, 0],
                "Change_when_ms": ["PM_TRIGGER", "FPA1"],
                "Plus_ms": [1000, 1000],
                "Minus_ms": [1000, 1000],
                "Link": ["AND", "AND"],
                "Const_at": [pd.NA, pd.NA],
            }
        ),
        pd.Series([0, 1, 1, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1),
        True,
    ),
    (
        pd.DataFrame(
            {
                "Signal_prefix": "signal",
                "ST_EE_Signal_Name": [_SIGNAL_1, _SIGNAL_1],
                "Change_from": [1, 0],
                "Change_to": [0, 1],
                "Change_when_ms": ["PM_TRIGGER", "FPA1"],
                "Plus_ms": [1000, 2000],
                "Minus_ms": [1000, 1000],
                "Link": ["AND", "AND"],
                "Const_at": [pd.NA, pd.NA],
            }
        ),
        pd.Series([1, 0, 0, 0, 1, 1, 1, 1, 1, 1], name=_SIGNAL_1),
        True,
    ),
]


@pytest.mark.parametrize(("criteria_df", "signal", "expected_result"), _PARAMS_CRITERIA_OR + _PARAMS_CRITERIA_AND)
def test_linked_criteria(criteria_df, signal, expected_result):
    with patch("lhcsmee.checks.digital_signals.criteria._get_fpa1_fpa2", return_value=(_FPA_1, _FPA_2)):
        criteria = Criteria(criteria_df, "source", "circuit_type")
        result = criteria.apply(False, [signal], [signal])
    assert result[0] == expected_result


_PARAMS_FPA1_FPA2 = [
    (
        [
            pd.Series([0, 0, 0, 0, 0, 0, 1, 1, 1, 1], name="ST_REC_FPA1"),
            pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1),
        ],
        6,
        None,
    ),
    (
        [
            pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name=_SIGNAL_1),
            pd.Series([0, 0, 0, 0, 1, 1, 1, 1, 1, 0], name="ST_REC_FPA2"),
        ],
        4,
        None,
    ),
    (
        [
            pd.Series([0, 0, 0, 0, 0, 0, 1, 1, 1, 1], name=_SIGNAL_1),
            pd.Series([0, 0, 0, 0, 1, 1, 1, 1, 1, 1], name=_SIGNAL_2),
        ],
        None,
        None,
    ),
    (
        [
            pd.Series([0, 0, 0, 0, 0, 0, 1, 1, 1, 1], name="ST_REC_FPA1"),
            pd.Series([0, 0, 0, 0, 1, 1, 1, 1, 1, 1], name="ST_REC_FPA2"),
        ],
        4,
        6,
    ),
    (
        [
            pd.Series([0, 0, 0, 0, 0, 0, 1, 1, 1, 1], name="ST_REC_FPA1"),
            pd.Series([0, 0, 0, 0, 1, 1, 1, 1, 1, 1], name="ST_REC_FPA2"),
        ],
        4,
        6,
    ),
    (
        [
            pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name="ST_REC_FPA1"),
            pd.Series([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], name="ST_REC_FPA2"),
        ],
        None,
        None,
    ),
]


@pytest.mark.parametrize(("signals", "expected_fpa1", "expected_fpa2"), _PARAMS_FPA1_FPA2)
def test_get_fpa1_fpa2(signals, expected_fpa1, expected_fpa2):
    from lhcsmee.checks.digital_signals.criteria import _get_fpa1_fpa2

    fpa1, fpa2 = _get_fpa1_fpa2(signals)
    assert fpa1 == expected_fpa1
    assert fpa2 == expected_fpa2
