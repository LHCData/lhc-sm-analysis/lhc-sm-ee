import pathlib

import papermill
import pytest


@pytest.mark.parametrize(
    ("notebook_name", "notebook_parameters"),
    [
        (
            "13kAEE_PM_Analysis.ipynb",
            {"start": "2024-06-12 14:00:00.000", "end": "2024-06-12 16:00:00.000", "interactive": False},
        ),
        (
            "13kAEE_PM_Analysis.ipynb",
            {"start": "2023-10-19 09:00:00.000", "end": "2023-10-19 09:00:00.000", "interactive": False},
        ),
        (
            "opening_statistics_13kAEE.ipynb",
            {"circuit_name": "RQF.A23", "start": "2020-11-01 01:00:00.000", "end": "2024-08-01 01:00:00.000"},
        ),
        (
            "600AEE_PM_Analysis.ipynb",
            {
                "circuit_name": "RQTD.A12B2",
                "t_start": "2023-03-21 06:58:49.227000000",
                "t_end": "2023-03-21 07:25:18.304000000",
                "is_sof": True,
            },
        ),
        (
            "600AEE_PM_Analysis.ipynb",
            {
                "circuit_name": "RSF1.A56B1",
                "t_start": "2022-02-04 19:31:00.096000000",
                "t_end": "2022-02-04 19:47:21.747000000",
                "is_sof": False,
            },
        ),
        (
            "600AEE_PM_Analysis.ipynb",
            {
                "circuit_name": "RCS.A81B2",
                "t_start": "2024-10-10 05:50:00.087000000",
                "t_end": "2024-10-10 05:55:32.395000000",
                "is_sof": False,
            },
        ),
        ("HWC_13kA_EE_PM_LIST_CCC.ipynb", {"start": "2024-06-12 14:00:00.000", "end": "2024-06-12 16:00:00.000"}),
    ],
)
def test_notebook(notebook_name: str, notebook_parameters: dict):
    notebook_path = pathlib.Path(__file__).parent.parent.parent / "src" / "lhcsmee" / "notebooks" / notebook_name
    report_path = pathlib.Path(__file__).parent.parent.parent / "reports" / f"report_{notebook_name}"

    papermill.execute_notebook(notebook_path, report_path, notebook_parameters)
